var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";
var authorization = "Basic " + btoa(username + ":" + password);


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
 /* global $*/
 /* global Chart */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
var vsiEhrji = ["", "", ""];
function generirajPodatke() {
  vsiEhrji = ["eeb91c69-5de2-48ee-a5f0-b5a090c77731","5e992f6e-5d6b-467f-9e48-5bbaf6de2eca","c98d00d8-8c5c-4fca-9acc-ce7963212521"];
  $("#generiranjeSporocilo").text("Generiranje podatkov je bilo uspešno. Izberite osebo iz padajočega menija");
 }

var uporabniki = [
        {"ime":"Shaquille", "priimek":"O'Neal", "datumRojstva": "1972-03-06T19:30", "starost": 47, "spol": "moski"}, 
        {"ime":"Serena", "priimek":"Williams", "datumRojstva": "1981-09-26T19:30", "starost": 37, "spol": "zenska"},
        {"ime":"Janez", "priimek":"Novak", "datumRojstva": "1999-05-01T4:20",  "starost": 20, "spol": "moski"}];
//Dropdown
var chartKalorij=null;
var chartMacrosov=null;
var chart=null;
var setup=0;

window.onload = function() {
  $('#preberiObstojeciVitalniZnak').change(function() {
    if ($("#preberiObstojeciVitalniZnak :selected").val() === "Shaq") $("#EhrZaMeritve").val(vsiEhrji[0]);
    else if ($("#preberiObstojeciVitalniZnak :selected").val() === "Serena") $("#EhrZaMeritve").val(vsiEhrji[1]);
    else if ($("#preberiObstojeciVitalniZnak :selected").val() === "Janez") $("#EhrZaMeritve").val(vsiEhrji[2]);
  }); 
  
  $('#preberiEhrIdZaVitalneZnake').change(function() {
    if ($("#preberiEhrIdZaVitalneZnake :selected").val() === "Shaq") $("#meritveVitalnihZnakovEHRid").val(vsiEhrji[0]);
    else if ($("#preberiEhrIdZaVitalneZnake :selected").val() === "Serena") $("#meritveVitalnihZnakovEHRid").val(vsiEhrji[1]);
    else if ($("#preberiEhrIdZaVitalneZnake :selected").val() === "Janez") $("#meritveVitalnihZnakovEHRid").val(vsiEhrji[2]);
  });
}
        
var dataSet = {
  "datum": [] ,"teza": [], "visina": []
}
var dnevneKalorije = [0, 0];
var macros = [0, 0, 0];
var kljuc = "tjemxbRrVOcp1E7vSehrkm5Lwyh02CW6mgOBBPzR";

function narisiGrafTeze() {
//Graf meritve tež
  var ctx = document.getElementById('myChart').getContext('2d');
  chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',

    // The data for our dataset
    data: {
        labels: dataSet.datum,
        datasets: [{
            label: 'Meritev telesne teže',
            borderColor: 'rgb(255, 80, 80)',
            fill: false,
            data: dataSet.teza
        }]
    },

    // Configuration options go here
    options: {}
});
}
function grafKalorij() {
// Prikaz dnevnih kalorij
  var ctx = document.getElementById('kalorijeChart').getContext('2d');
  chartKalorij = new Chart(ctx, {
    // The type of chart we want to create
    type: 'horizontalBar',
    //options
    // The data for our dataset
    data: {
        labels: ['Priporočen vnos', "Presekoračitev"],
        datasets: [{
            label: 'Energijska vrednost (cal)',
            borderWidth: 2,
            backgroundColor: ['#b3ffb3', "#ffc2b3"],
            borderColor: ['rgb(0, 204, 0)', "#ff3300"],
            data: dnevneKalorije
        }]
    },

    // Configuration options go here
    options: {
      scales: {
            xAxes: [{
                ticks: {
                    maxTicksLimit:10,
                    suggestedMax:1800,
                    stepSize:200,
                    suggestedMin:0
                   
                }
            }]
        }
    }
}); 
}

function grafMacrosov() {
// Prikaz macrosov
var ctx = document.getElementById('macroChart').getContext('2d');
chartMacrosov = new Chart(ctx, {
    // The type of chart we want to create
    type: 'horizontalBar',

    // The data for our dataset
    data: {
        labels: ['Ogljikovi hidrati', 'Beljakovine', 'Maščobe'],
        datasets: [{
            label: 'Količina (g)',
            borderWidth: 2,
            backgroundColor: ["#ffffb3", "#b3f0ff", "#ffd9b3"],
            borderColor: ["#ffff66", "#00ccff", "#ff9933"],
            data: macros
        }]
    },

    // Configuration options go here
    options: {}
});
}

//Prikaz meritev na grafu
var napaka = 0;
function validateId (id,callback) {
  var searchData = [
    {key: "ehrId", value: $("#meritveVitalnihZnakovEHRid").val()}
];
$.ajax({
    url: baseUrl + "/demographics/party/query",
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(searchData),
    headers: {
        "Authorization": authorization
    },
    success: function (res) {
        if (res == null || res.length == 0) {
          $("#preberiMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo label label-danger fade-in'>Vnesite veljaven EhrId");
        }
        callback();
    }
});
}
//document.getElementById("l").addEventListener("click", function()  {
function pridobivanjePodatkov() {
validateId($("#meritveVitalnihZnakovEHRid").val(),function(){
});
if (napaka == 0 && $("#meritveVitalnihZnakovEHRid").val() != "") {
  
  $("#preberiMeritveVitalnihZnakovSporocilo").html("");
  $("#ime").text("Ime: ");
  $("#priimek").text("Priimek: ");
  $("#starost").text("Starost: ");
  $("#kalorije").text("Priporočen dnevni vnos kalorij: ");
  var ehrId = $("#meritveVitalnihZnakovEHRid").val();
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + "weight",
    type: 'GET',
    headers: {
        "Authorization": authorization
    },
    success: function (res) {
      dataSet ={"datum": [] ,"teza": [], "visina":dataSet.visina}
        for (var i in res) {
            dataSet.datum[i] = (res[i].time).split("T", 1);
            dataSet.teza[i] = res[i].weight;
        }
          if(setup==0){
            setup++;
          }else chart.destroy();
          narisiGrafTeze();
    }
  });
  $.ajax({
    url: baseUrl + "/view/" + ehrId + "/" + "height",
    type: 'GET',
    headers: {
        "Authorization": authorization
    },
    success: function (res) {
        for (var i in res) {
            dataSet.visina[i] = res[i].height;
        }
    }
  });
  
  var searchData = [
      {key: "ehrId", value: ehrId}
  ];
  $.ajax({
      url: baseUrl + "/demographics/party/query",
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(searchData),
      headers:{
          "Authorization": authorization
      },
      success: function (res) {
          for (i in res.parties) {
              var party = res.parties[i];
              $("#ime").append(party.firstNames);
              $("#priimek").append(party.lastNames);
              var starost = 2019 - party.dateOfBirth.split("-", 1);
              $("#starost").append(starost);
              dnevneKalorije[0] = (10*dataSet.teza[0]) + (6.25*dataSet.visina[0]) - (5*starost) + 5;
              dnevneKalorije[0] = Math.ceil(dnevneKalorije[0]);
              $("#kalorije").append(dnevneKalorije[0]);
              macros[0] = macros[1] = macros[2] = 0;
              dnevneKalorije[1] = 0;
              $("#presegaMejo").html("");
              $("#presegaMejo2").html("");
              console.log("Grafi?");
              
              if(setup==1){
                  console.log("Narisan graf");
                  grafKalorij();
                  grafMacrosov();
                  setup++
              }
              else {
                console.log("Posodobljen graf");
                chartKalorij.update();
                chartMacrosov.update();
              }
              
          }
      }
  });
  var cdsUrl = 'https://rest.ehrscape.com/thinkcds/rest/v1';

      return $.ajax({
          url: cdsUrl + "/guide/BMI.Calculation.v.1/execute/" + ehrId + '/query',
          type: 'POST',
          headers: {
              "Authorization": authorization
          },
          success: function (data) {
              if (data instanceof Array) {
                  if (data[0].hasOwnProperty('results')) {
                      data[0].results.forEach(function (v, k) {
                          if (v.archetypeId === 'openEHR-EHR-OBSERVATION.body_mass_index.v1') {
                              var rounded = Math.round(v.value.magnitude * 100.0) / 100.0;
                              $("#bmi").text("Bmi: "+ rounded);
                          }
                      })
                  }
              }
          }
      });
} else napaka = 0;
}

//document.getElementById("dodajVitalneZnake").addEventListener("click", function()  {
function dodajVitalneZnake() {
	var ehrId = $("#EhrZaMeritve").val();
	var datumInUra = $("#dodajVitalnoDatumInUra").val();
	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();

	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
	} else {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "Janja Novak"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": authorization
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'> Dodajanje vitalnih znakov je bilo uspešno");
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
	}
}

// document.getElementById("izbiraHrane").addEventListener("click", function () {
function izbiraHrane(){
  var razneHrane = {
  "Pizza": "21272", "Burger": "21250", "Solata s piščancem": "21376", "Makaroni": "45236230",
  "Taco": "21260", "Bomboni": "45147101", "Nuttela": "45081187", "Juha": 	"45283249", "Sendvič": "45221761",
  "Torta": "45149325", "Sladoled":"19270", "Snickers":"45248489", "Kruh":"18033"
  }
  var izbira = $("#moznostiHrane :selected").text();
  var nbdno = razneHrane[izbira];
  $.ajax({
    url: "https://api.nal.usda.gov/ndb/reports/?api_key="+ kljuc +"&ndbno="+ nbdno,
    type: 'GET',
    dataType: 'json',
    contentType: "application/x-www-form-urlencoded; charset=UTF-8",
    success: function (res) {
      console.log("1");
      var skr = res.report.food.nutrients;
        $("#cal").text(skr[1].value+ " kcal");
        $("#ogl").text(skr[6].value+ " g");
        $("#prot").text(skr[3].value+ " g");
        $("#fat").text(skr[4].value+ " g");
      macros[0] += +skr[6].value;
      macros[1] += +skr[3].value;
      macros[2] += +skr[4].value;
      dnevneKalorije[0] -= skr[1].value;
      if (dnevneKalorije[0] < 0) {
        $("#presegaMejo").html("<span class='obvestilo label label-danger fade-in'>Presegli ste priporočen dnevni vnos");
        dnevneKalorije[1] -= dnevneKalorije[0];
        dnevneKalorije[0] = 0;
      }
      chartKalorij.update();
      chartMacrosov.update();
    }
  });
}	

// document.getElementById("vpisKalorij").addEventListener("click", function () {
function vpisovanjeKalorij() {
  dnevneKalorije[0] -= $("#dodajKalorije").val();
      if (dnevneKalorije[0] < 0) {
        $("#presegaMejo2").html("<span class='obvestilo label label-danger fade-in'>Presegli ste priporočen dnevni vnos");
        dnevneKalorije[1] -= dnevneKalorije[0];
        dnevneKalorije[0] = 0;
      }
      chartKalorij.update();
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
